# Apertimes Parser

This repo contains a set of tools to help managing Apertis related tasks

apertimes-parser.py: Parses the output or apertimes reports and creates text files with the list of tasks in progress
process-tasks.py: Process a test file with tasks in progress and allows to easily set Phabricator tags and parent tasks