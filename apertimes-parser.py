#!/usr/bin/python3
import argparse
import json
import os
import sys
from os.path import join

APERTIMES_REPORT='/home/wlozano/apertis/others/apertis-report-website'

class ApertimesParser:
    def __init__(self, report_dir, separator, verbose, all_tasks):
        self.report_dir = report_dir
        self.separator = separator
        self.verbose = verbose
        self.all_tasks = all_tasks

    def list_apertimes_report(self):
        data_dir = join(self.report_dir, 'data')
        os.system(f'ls -1 {data_dir}')

    def update_apertimes_report(self):
        os.system(f'cd {self.report_dir}; git fetch; git rebase')

    def get_latest_report_file(self, project):
        listdir = os.listdir(join(self.report_dir, 'data'))
        listdir.sort()
        listdir = [x for x in listdir if x.find(project) != -1]
        report_file =  listdir[-1]

        return report_file

    def load_report_file(self, report_file):
        self.report = json.load(open(join(self.report_dir, 'data', report_file)))

    def dump_task(self, task_type, task):
        sep = self.separator
        if not self.all_tasks:
            if task['id'] == 0 or task['description'] == 'Unknown':
                return
        if self.verbose == 1:
            print(task['id'])
        elif self.verbose  == 2:
            print(f"{task['id']}{sep}{task_type}{sep}{task['description']}")
        elif self.verbose == 3:
            points = None
            if task['spent_points']:
                points = "{:.2f}".format(float(task['spent_points']))
            print(f"{task['id']}{sep}{task_type}{sep}{points}{sep}{task['description']}")
        elif self.verbose == 4:
            url = 'https://phabricator.apertis.org/T' + str(task['id'])
            points = None
            if task['spent_points']:
                points = "{:.2f}".format(float(task['spent_points']))
            print(f"{url}{sep}{task_type}{sep}{points}{sep}{task['description']}")

    def dump_task_table(self, task_type = None):
        for p in self.report['projects']:
            for tt in p['task_types']:
                if task_type and tt['name'] != task_type:
                    continue
                for t in tt['tasks']:
                    self.dump_task(tt['name'], t)
                    for st in t['subtasks']:
                        self.dump_task(tt['name'], st)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-a","--all-tasks", action='store_true', help="list all tasks")
    parser.add_argument("-d","--report-dir", default=APERTIMES_REPORT, help="apertimes report dir")
    parser.add_argument("-p","--project", help="project")
    parser.add_argument("-r","--report-file", help="report file")
    parser.add_argument("-s","--separator", default=' ', help="separator")
    parser.add_argument("-t","--task-type", help="task type")
    parser.add_argument("-v","--verbose", type=int, default=2, help="verbose")

    parser.add_argument("action", help='action to execute')

    args = parser.parse_args()

    apertimes_parser = ApertimesParser(args.report_dir, args.separator, args.verbose, args.all_tasks)

    if args.action == 'dump':
        if args.project == None:
            print('Missing project, please specify')
            sys.exit()
        report_file = args.report_file
        if not report_file:
            report_file = apertimes_parser.get_latest_report_file(args.project)
        apertimes_parser.load_report_file(report_file)
        apertimes_parser.dump_task_table(args.task_type)
    elif args.action == 'list':
        apertimes_parser.list_apertimes_report()
    elif args.action == 'update':
        apertimes_parser.update_apertimes_report()
