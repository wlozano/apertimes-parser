#!/usr/bin/python3
import argparse
import os
import tempfile
from os.path import join

OPEN = 0
CLOSED = 1

PHABTOOL='/home/wlozano/apertis/infrastructure/phabtool/phabtool.py'

def read_tasks(task_file):
    return open(task_file).read().strip().split('\n')

def filter_task_number(tasks):
    tasks = [x.split(' ')[0] for x in tasks]
    tasks = [x for x in tasks if x != '0']
    return tasks

def create_tmp_file(buffer):
    tmp = tempfile.NamedTemporaryFile(mode='w+', delete=False)
    tmp.write(buffer)
    tmp.close()
    return tmp.name

def filter_status(tasks, status):
    tasks2 = []
    closed = ['Resolved', 'Closed']
    for t in tasks:
        s = t.split(' ')[1]
        is_closed = s in closed
        if status == OPEN and not is_closed:
            tasks2.append(t)
        elif status == CLOSED and is_closed:
            tasks2.append(t)
    return tasks2

def filter_umbrella_tasks(tasks):
    tasks = [x for x in tasks if x.find('P000 Build and integration baseline') == -1]
    tasks = [x for x in tasks if x.find('P001 Umbrella for Not-Warranty bugs') == -1]
    tasks = [x for x in tasks if x.find('P050 Support') == -1]

    return tasks

def format_task(tasks):
    return tasks

def filter_project(tasks, project, reverse = False):
    if not project.endswith('*'):
        tasks2 = [x for x in tasks if x.lower().find(f'#{project.lower()} ') != -1]
    else:
        tasks2 = [x for x in tasks if x.lower().find(f'#{project.lower()[:-1]}') != -1]

    if not reverse:
        return tasks2
    
    tasks3 = [x for x in tasks if x not in tasks2]

    return tasks3

def remove_projects(task):
    pos = task.find('#')
    if pos != 1:
        return task[:pos]
    else:
        return task

def run_phabtool_raw(*argv):
    cmd_args = ' '.join(argv)
    cmd = f'{PHABTOOL} {cmd_args}'
    os.system(cmd)

def run_phabtool(tasks, action, arg1):
    for t in tasks:
        cmd = f'{PHABTOOL} {action} {t} "{arg1}"'
        os.system(cmd)

def dump_tasks(tasks, verbose = 3):
    for t in tasks:
        if verbose == 1:
            print(t.split(' ')[0])
        elif verbose == 2:
            t = remove_projects(t)
            print(t)
        else:
            print(t)

if __name__ == '__main__':

    action_help = '''
    Executes action on tasks:

        filter [file]: Filter tasks by projects
        get-tasks [file]: Get tasks information from ids read from file
        set-parent [file] [id1]: Set id1 as parent of tasks
        set-project [file] [id1]: Set id1 as project
    '''

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=action_help)
    parser.add_argument("-c","--closed", action='store_true', help="closed")
    parser.add_argument("-n","--no-project", help="project")
    parser.add_argument("-o","--open", action='store_true', help="open")
    parser.add_argument("-p","--project", help="project")
    parser.add_argument("-u","--umbrella", action='store_true', help="project")
    parser.add_argument("-v","--verbose", type=int, default=2, help="verbose")

    parser.add_argument("action", help='action to execute')
    parser.add_argument("file", help='file to act on')
    parser.add_argument("arg1", nargs='?', default='', help='group/task to act on')

    args = parser.parse_args()

    if args.action == 'filter':
        tasks = read_tasks(args.file)
        tasks = format_task(tasks)
        if args.umbrella:
            tasks = filter_umbrella_tasks(tasks)
        if args.no_project:
            tasks = filter_project(tasks, args.no_project, True)
        if args.project:
            tasks = filter_project(tasks, args.project)
        if args.open:
            tasks = filter_status(tasks, OPEN)
        if args.closed:
            tasks = filter_status(tasks, CLOSED)
        dump_tasks(tasks, args.verbose)
    elif args.action == 'get-tasks':
        tasks = read_tasks(args.file)
        tasks = filter_task_number(tasks)
        buffer = '\n'.join(tasks)
        tasks_file = create_tmp_file(buffer)
        out_file = create_tmp_file('')
        run_phabtool_raw('-v 3', 'get-tasks', tasks_file, '>', out_file)
        tasks = read_tasks(out_file)
        tasks = format_task(tasks)
        dump_tasks(tasks, 3)
    elif args.action == 'set-parent':
        tasks = read_tasks(args.file)
        tasks = filter_task_number(tasks)
        run_phabtool(tasks, 'set-task-parent', args.arg1)
    elif args.action == 'set-project':
        tasks = read_tasks(args.file)
        tasks = filter_task_number(tasks)
        run_phabtool(tasks, 'set-task-project', args.arg1)
